/**
 * 主类使用方式
 *
 * @author yyq
 */
// 【引入外部组件】
// layMin详情：https://gitee.com/yuanyongqiang/lay-min
// treeSelect详情：https://wujiawei0926.gitee.io/treeselect/docs/doc.html
layui.config({
	base: "dist/models/",
}).extend({
	layMin: "layMin/layMin.min",
	treeSelect: "treeSelect"
});
var $, element, layMin, util, form, dropdown, treeSelect;
// layui 组件引入
layui.use(['element', 'layMin', 'util', 'form', 'dropdown', 'treeSelect'], function() {
	$ = layui.$;
	element = layui.element;
	layMin = layui.layMin;
	util = layui.util;
	form = layui.form;
	dropdown = layui.dropdown;
	treeSelect = layui.treeSelect;

	// 回到顶部
	util.fixbar({
		bar1: false,
		click: function(type) {
			console.log(type);
		}
	});


	// 移动端显示更多
	dropdown.render({
	    elem: '#mobile-all'
	    ,trigger: 'hover'
	    ,data: [
	    	{
	    		id: 'tips',
	    		title: "消息",
	    	},{
	    		id: 'theme',
	    		title: "主题更换",
	    	},
	    ]
	    ,click: function(obj, othis){
	    	if(obj.id == 'tips'){
	    		mainObj.messageTips();
	    	}else if(obj.id == 'theme'){
	    		configObj.openSettingTheme()
	    	}
	    }
	});

	// 主题存储/获取
//	var theme = {'theme': 16, 'menu': 2, 'page': true, 'logo': false, 'refresh': false, 'radius': true, 'anim': 2};
//	localStorage.setItem('lay_menu_theme', JSON.stringify(theme));
//
//	var value = localStorage.getItem('lay_menu_theme');
//	var theme = JSON.parse(value);


	// 初始化配置
	configObj.init({
		scrollTop: true, // 回到顶部
		layui: '2.6.8', // 请根据使用版本修改此处（必须三位数）
		search: '搜索...', // 布尔/字符串（字符串代表提示；如果设置为false，说明不需要显示，那么请删除index.html中的class="lay-tree-menu"标签）
		//iframe: true, // 是否开启iframe模式
		/*theme: {// 主题设置内容
			'theme': 16, // 主题样式
			'menu': 2, // 菜单类型
			'page': true, // 标签页
			'pageType': false, // 标签页圆角
			'pageIcon': false, // 标签页图标
			'logo': false, // 固定logo
			'refresh': false, // 点击菜单刷新
			'radius': true, // 选中菜单圆角
			'anim': 2, // 页面动画效果
			'leftScroll': true, // 菜单滚动条显示
			'leftCloseChild': false // 关闭其他展开子菜单
		}, */
	});

	// 加载菜单
	var data = [
		{
			"mid":101,
			"mname":"首页",
			"url": "page/home.html",
			"icon":"<i class='layui-icon lay-icon'>&#xe68e;</i>", // lay-icon 必须带，样式根据这个调整
			"children":[]
		},{
			"mid":102,
			"mname":"用户管理",
			"url": "page/user.html",
			"icon":"<i class='layui-icon lay-icon'>&#xe66f;</i>",
			"children":[]
		},{
			"mid":103,
			"mname":"系统管理",
			"url": "",
			"icon":"<i class='layui-icon lay-icon'>&#xe716;</i>",
			"children":[
				{
					"mid":10301,
					"mname":"角色管理",
					"url": "page/role.html",
					"icon":"<i class='layui-icon lay-icon'>&#xe60a;</i>",
					"children":[]
				},{
					"mid":10302,
					"mname":"结果页面",
					"url": "",
					"icon":"<i class='layui-icon lay-icon'>&#xe60a;</i>",
					"children":[
							{
							"mid":1030201,
							"mname":"成功页面",
							"url": "page/success.html",
							"icon":"<i class='layui-icon lay-icon'>&#xe60a;</i>",
							"children":[]
						},{
							"mid":1030202,
							"mname":"失败页面",
							"url": "page/fail.html",
							"icon":"<i class='layui-icon lay-icon'>&#xe60a;</i>",
							"children":[]
						}
					]
				}
			]
		},{
			"mid":104,
			"mname":"日志管理",
			"url": "",
			"icon":"<i class='layui-icon lay-icon'>&#xe621;</i>",
			"children":[
				{
					"mid":10401,
					"mname":"登录日志",
					"url": "page/log_login.html",
					"icon":"<i class='layui-icon lay-icon'>&#xe621;</i>",
					"children":[]
				},{
					"mid":10402,
					"mname":"操作日志",
					"url": "page/log_operate.html",
					"icon":"<i class='layui-icon lay-icon'>&#xe621;</i>",
					"children":[]
				}
			]
		},{
			"mid":105,
			"mname":"layer 弹框",
			"url": "page/layer.html",
			"icon":"<i class='layui-icon lay-icon'>&#xe638;</i>",
			"children":[]
		},{
			"mid":106,
			"mname":"按钮",
			"url": "page/btn.html",
			"icon":"<i class='layui-icon lay-icon'>&#xe638;</i>",
			"children":[]
		},{
			"mid":107,
			"mname":"Layui 新官网",
			"url": "https://layui.dev/",
			"urlType": 1, // 外部链接
			"icon":"<i class='layui-icon lay-icon'>&#xe609;</i>",
			"children":[]
		}
	];
	configObj.loadMenu(data);
	// 或
	// 请求加载数据，使用的时候把这个函数里面的请求数据方式改了就行，反正按正常格式获取数据即可
	//mainObj.loadMenu();

	// 主题设置回调
	configObj.themeCallback = function(data){
		console.log('主题设置：', JSON.stringify(data));
	}



});

// 当前页对象
var mainObj = {
	/** 右侧更多*/
	rightAll: function() {
		layMin.openLayer({
			elem: "body", // 在哪个容器弹出，默认body
			type: 1, // 弹框类型：1、内容，2 、html元素标签 3、iframe页面
			title: false, // 弹框标题，可设置false
			colseIcon: false, // 是否隐藏关闭按钮
			shade: 0.5, // 背景透明度0 ~ 1，可设置false
			shadeClose: true, // 点击遮罩关闭
			//time: 3000, // 自动关闭时间（毫秒）
			width: '300', // 弹框宽度
			height: '95%', // 弹框高度
			offset: {
				'right': '0px',
				'bottom': '0px',
				'left': 'unset',
				'top': 'unset'
			}, // 弹出位置，unset代表取消样式
			//fullScreen: true, // 是否全屏
			anim: 1, // 动画 0 ~ 5 ，可设置false
			btns: false,
			content: $('#lay-head-right-all').html(), // 内容
			close: function(index) {
				console.log(index);
				console.log("右上角图标关闭");
			},
			yes: function(index) {
				console.log("确认");
				layMin.closeLayer(index);
			},
			not: function(index) {
				console.log("关闭");
				layMin.closeLayer(index);
			},
			success: function(index, elem) {
				console.log("成功渲染");
				$(elem).css('border-radius', '0px');
			},
		})
	},
	/** 加载菜单数据*/
	loadMenu: function() {
		$.getJSON("js/data.json", function(data) {
			//configObj.iframe = true; // 可设置为iframe模式
			configObj.loadMenu(data);
		})
	},
	messageTips: function() {
		layMin.tips({msg: "消息提示", iconIndex: 4, time: 3});
	},
	personalData: function() {
		layMin.tips({msg: "点击个人资料", iconIndex: 0, time: 3});
	},
	setting: function() {
		layMin.tips({msg: "点击设置", iconIndex: 2, time: 3});
	},
	exitLogin: function() {
		layMin.confirm({
			title: '提示', // 弹框标题，可设置空值''，可设置false
			icon: 1, // 图标：0~2
			text: "您确定退出吗？", // 内容
			yes: function(index, elem) {
				layMin.tips({msg: "退出失败", iconIndex: 1, time: 3});
				layMin.closeLayer(index);
			},
		})
	},
}
