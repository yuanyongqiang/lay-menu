/**
 * 配置对象
 * [通用，无需改动]
 * @author yyq
 * @date 2024-05-16
 */
var configObj = {
	version: '3.2', // 当前版本
	layui: '2.6.8', // layui版本
	iframe: false, // 是否开启iframe容器
	bodyTab: 'body-tab', // 主体的标签页
	bodyTabTileHeight: 35, // 标签页的高度
	bodyTabTileWidth: 106, // 标签页的宽度
	menuList: [], // 菜单集合
	menuLastMid: null, // 菜单最后选中的mid（一般头部菜单使用）
	scrollTop: false, // 是否回到顶部
	theme: {'theme': 0, 'menu': 0, 'page': true, 'pageIcon': false, 'pageType': false, 'logo': false, 'refresh': false, 'radius': false, 'anim': 2}, // 主题设置内容
	themeCallback: null, // 主题设置后回调函数
	animArray: ['lay-anim-not', 'lay-anim-fadein', 'lay-anim-tb', 'lay-anim-bt', 'lay-anim-lr', 'lay-anim-rl'],
	minWidth: 1100, // 最小显示菜单宽度
	defMenu: {}, // 默认菜单（用于不关闭）
	// 初始化参数
	init: function(data){
		console.log("layMenu version：", configObj.version);
		// 参数设定
		if(data){
			for(var key in data){
				configObj[key] = data[key];
			}
		}
		// 是否开启菜单搜索
		if(data.search === false || data.search){
			configObj.search = data.search;
		}else{
			configObj.search = true;
		}
		// 菜单类型初始化
		configObj.setMenuTypeStyle();
		// 判断是否版本是否大于2.6.8，因为大于这个版本有些东西不兼容
		configObj.layVersion = configObj.replaceAll(configObj.layui, '.', '');
		// 初始化主题
		configObj.initTheme();
		// 自适应监听
		configObj.resize();
		$('.layui-layout-admin .layui-side').css('display', 'block');
		// 给左侧操作添加动画
		setTimeout(function(){
			var c = '.layui-layout-admin .layui-side-scroll,';
			c += '.layui-layout-admin .layui-side-scroll,';
			c += '.layui-layout-admin .layui-layout-left,';
			c += '.layui-layout-admin .layui-logo,';
			c += '.layui-layout-admin .layui-side,';
			c += '.layui-layout-admin .layui-side .layui-nav-tree,';
			c += '.layui-layout-admin .layui-body';
			$(c).addClass('lay-common-anim');
		}, 500)
	},
	replaceAll: function(str, s1, s2){
		var value = str.replace(s1, s2);
		if(value.indexOf(s1) != -1){
			return configObj.replaceAll(value, s1, s2);
		}
		return value;
	},
	/** 设置菜单类型的样式效果*/
	setMenuTypeStyle: function(){
		if(this.theme.menu == 1){
			$(".layui-side").css({'left': '-50px', 'width': '0px'});
			$(".layui-body").css('left', '0px');
			$(".lay-menu-navigation, .lay-hiden-menu").css('display', 'none');
		}else if(this.theme.menu == 2){
			$(".lay-menu-navigation").css('display', 'none');
			$(".lay-hiden-menu").css('display', 'block');
		}else{
			$(".lay-menu-navigation, .lay-hiden-menu").css('display', 'block');
		}
		if(this.theme.menu != 1){
			var w = $(window).width();
		    if(w < configObj.minWidth){
		    	$('.lay-hiden-menu').attr('lay-id', '');
		    }else{
		    	$('.lay-hiden-menu').attr('lay-id', 1);
		    }
		    configObj.hidenMenu('.lay-hiden-menu');
		}
	},
	/** 加载菜单*/
	loadMenu: function(list) {
		if(!list || list.length == 0){
			return console.log("没有菜单数据");
		}
		$('.lay-tab>.layui-tab-title li').each(function(){
			var layId = $(this).attr("lay-id");
			element.tabDelete(configObj.bodyTab, layId);
		})
		this.menuList = list;
		configObj.setMenuTypeStyle();
		$('.lay-head-menu, .lay-left-menu').empty();
		if(this.theme.menu == 1){
			this.showHeadMenu(list);
		}else if(this.theme.menu == 2){
			this.showHeadLeftMenu(list);
		}else{
			this.showLeftMenu(list);
		}
		if(configObj.search){
			var treeList = this.getTreeMenus(list);
			this.loadTreeSelect(treeList);
		}
	},
	/** 渲染菜单-头部菜单*/
	showHeadMenu: function(list) {
		var elem = '.lay-head-menu';
		for(var i = 0; i < list.length; i++) {
			var obj = list[i];
			var li = $('<li class="'+(i == 0 ? 'lay-this' : '')+'" id="lay-head-'+obj.mid+'"></li>');
			var all = '';
			if(obj.children && obj.children.length > 0) {
				all = '<i class="layui-icon layui-icon-down lay-common-anim"></i>';
			}
			obj.headMenuId = obj.mid;
			var a = $('<a lay-id="' + obj.mid + '" lay-name="' + obj.mname + '" lay-type="' + obj.urlType + '" lay-url="' + obj.url + '" lay-chil="'+(all?'1':'')+'"><span style="'+(all?'padding-right: 30px;':'')+'">' + obj.icon + '<cite>'+obj.mname+'</cite>'+all+'</span></a>');
			li.append(a);
			$(elem).append(li);
			if(all){
				configObj.getChilHeadMenu(obj.children, obj.mid);
				obj.child = obj.children;
				dropdown.render({
				    elem: '#lay-head-'+obj.mid
				    ,trigger: 'hover'
				    ,align: 'center'
				    ,data: obj.child
				    ,click: function(obj, othis){
				    	configObj.menuLastMid = obj.mid;
						configObj.setTabUrl(obj, null);
				    },
				});
			}
		}
		this.maxHeadMenu();
		this.eventClick(".lay-menu-left"); // 用于左侧菜单展开时的动画效果，无需动画效果可注释
		var homeObj = this.getHomePage(list[0]); // 设置默认首页
		this.defMenu = homeObj;
		this.swithTab(homeObj);
		if(this.scrollTop){
			this.scrollTopLabel();
		}
		// 点击页面显示
		$(elem).unbind('click');
		$(elem).on('click', 'li', function(){
			var cls = $(this).attr('class');
			if(cls.indexOf('lay-this') != -1){
				if(configObj.theme.page && configObj.theme.refresh){
	            	configObj.refreshPage();
	            }else{
	            	return;
	            }
			}
			$(elem).find('.lay-this').removeClass('lay-this');
			$(this).addClass('lay-this');
			if(cls.indexOf('lay-head-all') != -1 || $(this).find('a').attr('lay-chil')){
				return;
			}
			var layId = $(this).find('a').attr('lay-id');
			var layUrl = $(this).find('a').attr('lay-url');
			var layType = $(this).find('a').attr('lay-type');
			var layName = $(this).find('a').attr('lay-name');
			configObj.menuLastMid = layId;
			configObj.setTabUrl({
				mid: layId,
				url: layUrl,
				type: layType,
				mname: layName,
				headMenuId: layId
			}, null)
		});
	},
	/** 头部菜单最大显示多少，超出后显示点点*/
	maxHeadMenu: function(){
		var ulWidth = $('.lay-head-menu').children('li').length * 110;
		var w = window.innerWidth*700/1920;
		if(ulWidth > w){
			$('.lay-head-all').remove();
			var tempWidth = 0, tempList = [];
			$('.lay-head-menu li').each(function(){
				tempWidth += 110;
				if(tempWidth >= w){
					$(this).addClass('lay-hiden');
					var layId = $(this).find('a').attr('lay-id');
					var layUrl = $(this).find('a').attr('lay-url');
					var layType = $(this).find('a').attr('lay-type');
					var layName = $(this).find('a').attr('lay-name');
					tempList.push({id: layId, title: layName, mname: layName, url: layUrl, type: layType});
				}else{
					$(this).removeClass('lay-hiden');
				}
			})
			if(tempList.length > 0){
				var time = new Date().getTime();
				for(var i = 0; i < tempList.length; i++) {
					tempList[i].headMenuId = time;
					for(var j = 0; j < configObj.menuList.length; j++) {
						var obj = configObj.menuList[j];
						if(obj.mid == tempList[i].id){
							tempList[i].title = obj.mname;
							tempList[i].templet = obj.icon + '{{= d.title }}';
							configObj.getChilHeadMenu(obj.children, time);
							tempList[i].child = obj.children;
						}
					}
				}
				$('.lay-head-menu').append('<li class="lay-head-all" id="lay-head-'+time+'"><a><span><i class="layui-icon">&#xe65f;</i></span></a></li>');
				dropdown.render({
				    elem: '#lay-head-'+time
				    ,trigger: 'hover'
				    ,align: 'center'
				    ,data: tempList
				    ,click: function(obj, othis){
				    	configObj.menuLastMid = obj.mid;
						configObj.setTabUrl(obj, null);
				    },
				});
			}
		}
	},
	/** 获取头部对应的下拉子菜单*/
	getChilHeadMenu: function(list, id){
		if(!list){
			return;
		}
		for(var i = 0; i < list.length; i++) {
			var obj = list[i];
			obj.id = obj.mid;
			obj.title = obj.mname;
			obj.headMenuId = id;
			obj.templet = obj.icon + '{{= d.title }}';
			if(obj.children && obj.children.length > 0) {
				configObj.getChilHeadMenu(obj.children, id);
				obj.child = obj.children;
			}
		}
	},
	/** 渲染菜单-头部与左侧菜单*/
	showHeadLeftMenu: function(list) {
		var elem = '.lay-head-menu';
		for(var i = 0; i < list.length; i++) {
			var obj = list[i];
			var li = $('<li class="'+(i == 0 ? 'lay-this' : '')+'"></li>');
			var a = $('<a lay-id="' + obj.mid + '" lay-name="' + obj.mname + '" lay-type="' + obj.urlType + '" lay-url="' + obj.url + '"><span>' + obj.icon + '<cite>'+obj.mname+'</cite></span></a>');
			li.append(a);
			$(elem).append(li);
		}
		this.maxHeadLeftMenu();
		var chilList = this.getChilHeadLeftMenu(list, list[0].mid);
		this.showLeftMenu(chilList);
		// 点击切换子菜单
		$(elem).unbind('click');
		$(elem).on('click', 'li', function(){
			var cls = $(this).attr('class');
			if(cls.indexOf('lay-this') != -1){
				if(configObj.theme.page && configObj.theme.refresh){
	            	configObj.refreshPage();
	            }else{
	            	return;
	            }
			}
			$(elem).find('.lay-this').removeClass('lay-this');
			$(this).addClass('lay-this');
			if(cls.indexOf('lay-head-all') != -1){
				return;
			}
			$('.lay-tab>.layui-tab-title li').each(function(){
				var layId = $(this).attr("lay-id");
				element.tabDelete(configObj.bodyTab, layId);
			})
			var layId = $(this).find('a').attr('lay-id');
			var chilList = configObj.getChilHeadLeftMenu(configObj.menuList, layId);
			configObj.showLeftMenu(chilList);
			if($('.lay-hiden-menu').attr('lay-id') == 1){
				$('.lay-hiden-menu').attr('lay-id', '');
				configObj.hidenMenu('.lay-hiden-menu');
			}
		});
	},
	/** 头部与左侧菜单最大显示多少，超出后显示点点*/
	maxHeadLeftMenu: function(){
		var ulWidth = $('.lay-head-menu').children('li').length * 110;
		var w = window.innerWidth*700/1920;
		if(ulWidth > w){
			$('.lay-head-all').remove();
			var tempWidth = 0, tempList = [];
			$('.lay-head-menu li').each(function(){
				tempWidth += 110;
				if(tempWidth >= w){
					$(this).addClass('lay-hiden');
					var layId = $(this).find('a').attr('lay-id');
					var layUrl = $(this).find('a').attr('lay-url');
					var layType = $(this).find('a').attr('lay-type');
					var text = $(this).find('cite').text();
					tempList.push({id: layId, title: text, url: layUrl, type: layType});
				}else{
					$(this).removeClass('lay-hiden');
				}
			})
			if(tempList.length > 0){
				for(var i = 0; i < tempList.length; i++) {
					for(var j = 0; j < configObj.menuList.length; j++) {
						var obj = configObj.menuList[j];
						if(obj.mid == tempList[i].id){
							tempList[i].title = obj.mname;
							tempList[i].templet = obj.icon + '{{= d.title }}';
						}
					}
				}
				var time = new Date().getTime();
				$('.lay-head-menu').append('<li class="lay-head-all" id="lay-head-all-'+time+'"><a><span><i class="layui-icon">&#xe65f;</i></span></a></li>');
				dropdown.render({
				    elem: '#lay-head-all-'+time
				    ,trigger: 'hover'
				    ,align: 'center'
				    ,data: tempList
				    ,click: function(obj, othis){
				    	if(obj.urlType && obj.urlType == 1){
				    		return configObj.setTabUrl(obj, null);
				    	}
				    	$('.lay-tab>.layui-tab-title li').each(function(){
							var layId = $(this).attr("lay-id");
							element.tabDelete(configObj.bodyTab, layId);
						})
						var chilList = configObj.getChilHeadLeftMenu(configObj.menuList, obj.id);
						configObj.showLeftMenu(chilList);
						if($('.lay-hiden-menu').attr('lay-id') == 1){
							$('.lay-hiden-menu').attr('lay-id', '');
		    				configObj.hidenMenu('.lay-hiden-menu');
						}
				    },
				});
			}
		}
	},
	/** 获取头部对应的左侧子菜单*/
	getChilHeadLeftMenu: function(list, mid){
		var tempList = [];
		for(var i = 0; i < list.length; i++) {
			var obj = list[i];
			if(obj.mid == mid){
				if(obj.children && obj.children.length > 0) {
					tempList = obj.children;
				}else{
					tempList.push(obj);
				}
				return tempList;
			}
		}
		return tempList;
	},
	/** 渲染菜单-左侧菜单*/
	showLeftMenu: function(list) {
		var elem = '.lay-left-menu';
		$(elem).empty();
		for(var i = 0; i < list.length; i++) {
			var obj = list[i];
			var li = $('<li class="layui-nav-item lay-menu-mark"></li>');
			var events = ((obj.children && obj.children.length > 0) ? ' lay-chil="true"':'onmouseover="configObj.eventMouseover(\''+obj.mname+'\', '+obj.mid+')" onmouseout="configObj.eventMouseout()" onclick="configObj.setTabUrl(null, this)"');
			var a = $('<a class="lay-menu-a lay-tips-'+obj.mid+'" '+events+' href="javascript:;" onmouseover="configObj.eventMouseoverDropdown()"  lay-names="' + obj.mname + '"><div class="lay-text-div" style="margin-left: 0px;"><span class="lay-text" lay-id="' + obj.mid + '" lay-type="' + obj.urlType + '" lay-url="' + obj.url + '">' + obj.icon + '<cite>' + obj.mname + '</cite></span></div></a>');
			li.append(a);
			if(obj.children && obj.children.length > 0) {
				this.chilLeftMenu(obj.children, li, 20, obj.mname);
				// 用于显示菜单收起后的下拉菜单
				var a1 = $('<div class="lay-dropdown-div" id="lay-dropdown-'+obj.mid+'"></div>');
				li.append(a1);
			}
			$(elem).append(li);
			if(obj.children && obj.children.length > 0) {
				dropdown.render({
				    elem: '#lay-dropdown-'+obj.mid
				    ,trigger: 'hover'
				    ,align: 'right'
				    ,data: obj.children
				    ,click: function(obj, othis){
				    	configObj.setTabUrl(obj, null);
				    }
				});
			}else{
				obj.children = null;
			}
		}
		element.render('nav');
		this.eventClick(elem); // 用于左侧菜单展开时的动画效果，无需动画效果可注释
		var homeObj = this.getHomePage(list[0]); // 设置默认首页
		this.defMenu = homeObj;
		this.swithTab(homeObj);
		if(this.scrollTop){
			this.scrollTopLabel();
		}
	},
	/** 渲染子菜单-左侧菜单*/
	chilLeftMenu: function(list, li, left, names) {
		var dl = $('<dl class="layui-nav-child"></dl>');
		for(var i = 0; i < list.length; i++) {
			var obj = list[i];
			obj.id = obj.mid;
			obj.title = obj.mname;
			obj.templet = obj.icon + '{{= d.title }}';
			var dd = $('<dd class="lay-menu-mark"></dd>');
			var a = $('<a class="lay-menu-a" lay-names="' + names + ',' + obj.mname + '" '+((obj.children && obj.children.length > 0) ? ' lay-chil="true"':'onclick="configObj.setTabUrl(null, this)"')+'><div class="lay-text-div" style="margin-left:' + left + 'px;"><span class="lay-text" lay-id="' + obj.mid + '" lay-type="' + obj.urlType + '" lay-url="' + obj.url + '">' + obj.icon + '<cite>' + obj.mname + '</cite></span></div></a>');
			dd.append(a);
			if(obj.children && obj.children.length > 0) {
				this.chilLeftMenu(obj.children, dd, left + 20, names + ',' + obj.mname);
				obj.child = obj.children;
			}else{
				obj.children = null;
			}
			dl.append(dd);
		}
		li.append(dl);
	},
	/** 获取主页（默认第一个）*/
	getHomePage: function (data) {
	    if (data.children && data.children.length > 0) {
	        return configObj.getHomePage(data.children[0]);
	    }
	    return data;
	},
	/** 获取所有下拉菜单*/
	getTreeMenus: function (list, id){
		var arr = [];
		for (var i = 0; i < list.length; i++) {
			var obj = list[i];
			var mid = !id ? obj.mid : id;
			var tempObj = {"id": obj.mid, "name": obj.mname, "open": true, "u": obj.url, "type": obj.urlType, "headMenuId": mid};
			if (obj.children && obj.children.length > 0) {
				var children = configObj.getTreeMenus(obj.children, mid);
				tempObj['children'] = children;
			}
			arr.push(tempObj);
		}
		return arr;
	},
	/** 渲染下拉树形菜单*/
	loadTreeSelect: function (list){
		treeSelect.destroy('lay-menu-treeselect');
		treeSelect.render({
	        elem: '#lay-menu-treeselect',
	        list: list,
	        type: 'get',
	        placeholder: (typeof configObj.search === "string") ? configObj.search : '请输入菜单搜索',
	        search: true,
	        style: {
	            folder: {
	                enable: false // 图标
	            },
	            line: {
	                enable: true  // 线
	            }
	        },
	        click: function(d){
	        	var data = d.current;
	        	if(!data.children || data.children.length < 1){
	            	configObj.setTabUrl(data, null);
	            }
	        },
	        success: function (d) {
	        	// 选中节点，根据id筛选
	    		//treeSelect.checkNode('lay-menu-treeselect', id);
	        	// 刷新树结构
	        	//treeSelect.refresh('lay-menu-treeselect');
	        }
	    });
	},
	/** 监听点击事件*/
	eventClick: function(elem) {
		// 向左标签显示
		$("#lay-icon-prev").unbind('click');
		$("#lay-icon-prev").click(function(){
			var w = $('.lay-tab>.layui-tab-title').width();
			var count = $('.lay-tab>.layui-tab-title').scrollLeft();
			var num = count - w;
			num = (num < 0) ? 0 : num;
			var inter = setInterval(function(){
				if(count > num){
					count = count - 10;
					if(count <= num){
						$('.lay-tab>.layui-tab-title').scrollLeft(num);
						clearInterval(inter);
					}
				}
				if(count <= num){
					clearInterval(inter);
				}
				$('.lay-tab>.layui-tab-title').scrollLeft(count);
			}, 10)
		})
		// 向右标签显示
		$("#lay-icon-next").unbind('click');
		$("#lay-icon-next").click(function(){
			var w = $('.lay-tab>.layui-tab-title').width();
			var count = $('.lay-tab>.layui-tab-title').scrollLeft();
			var num = count + w;
			var inter = setInterval(function(){
				if(count < num){
					count = count + 10;
					if(count >= num){
						$('.lay-tab>.layui-tab-title').scrollLeft(num);
						clearInterval(inter);
					}
				}
				if(count >= num){
					clearInterval(inter);
				}
				$('.lay-tab>.layui-tab-title').scrollLeft(count);
			}, 10)
		})
		// 关闭标签页操作
		dropdown.render({
		    elem: '#lay-icon-down'
		    ,trigger: 'hover'
		    ,data: [
		    	{
		    		id: 0,
		    		title: "刷新当前",
		    		templet: '<i class="layui-icon lay-icon">&#xe9aa;</i>{{= d.title }}',
		    	},{
		    		id: 1,
		    		title: "关闭其他",
		    		templet: '<i class="layui-icon lay-icon">&#xe616;</i>{{= d.title }}',
		    	},{
		    		id: 2,
		    		title: "关闭左边",
		    		templet: '<i class="layui-icon lay-icon">&#xe603;</i>{{= d.title }}',
		    	},{
		    		id: 3,
		    		title: "关闭右边",
		    		templet: '<i class="layui-icon lay-icon">&#xe602;</i>{{= d.title }}',
		    	},
		    ]
		    ,click: function(obj, othis){
		    	if(obj.id == 0){
		    		configObj.refreshPage();
		    	}else if(obj.id == 1){
		    		$('.lay-tab>.layui-tab-title li').each(function(){
						var cls = $(this).attr("class")
						var layId = $(this).attr("lay-id");
						if (cls.indexOf('layui-this') == -1 && layId != configObj.defMenu.mid) {
							element.tabDelete(configObj.bodyTab, layId);
						}
					})
		    	}else if(obj.id == 2){
		    		var count = -1;
		    		$('.lay-tab>.layui-tab-title li').each(function(){
						var cls = $(this).attr("class")
						var layId = $(this).attr("lay-id");
						if (cls.indexOf('layui-this') != -1) {
							count = 1;
						}
						if(count == -1 && layId != configObj.defMenu.mid){
							element.tabDelete(configObj.bodyTab, layId);
						}
					})
		    	}else if(obj.id == 3){
		    		var count = -1;
		    		$('.lay-tab>.layui-tab-title li').each(function(){
						var cls = $(this).attr("class")
						var layId = $(this).attr("lay-id");
						if(count == 1 && layId != configObj.defMenu.mid){
							element.tabDelete(configObj.bodyTab, layId);
						}
						if (cls.indexOf('layui-this') != -1) {
							count = 1;
						}
					})
		    	}
		    }
		});
		if(configObj.layVersion > 268){
			if(configObj.layVersion < 290){
				// 点击左侧菜单a标签
				$(elem).unbind('click');
				$(elem).on('click', '.lay-menu-a', function() {
					if(configObj.openLeftMenuState){
						return;
					}
					var layChil = $(this).attr('lay-chil');
					if(!layChil){
						return;
					}
					var layId = $('.lay-hiden-menu').attr('lay-id');
					if(layId == 1){
						$('.layui-side').find(".layui-nav-expand").removeClass('layui-nav-expand');
					}else if(configObj.theme.leftCloseChild){ // 关闭其他展开的菜单
						var pa = $(this).parent()[0];
						$(pa).siblings().each(function(){
							if($(this).attr('class').indexOf('layui-nav-expand') != -1){
								var _li_this = this;
								var _a_this = $(_li_this).find('.lay-menu-a')[0];
								var a_child = $(_a_this).next();
								a_child.attr('style', '');
								a_child.find('.layui-nav-expand').removeClass('layui-nav-expand');
								$(_li_this).removeClass('layui-nav-expand');
							}
						})
					}
				})
			}else{
				// 点击左侧菜单a标签
				$(elem).unbind('click');
				$(elem).on('click', '.lay-menu-a', function() {
					if(configObj.openLeftMenuState){
						return;
					}
					var layChil = $(this).attr('lay-chil');
					if(!layChil){
						return;
					}
					var layId = $('.lay-hiden-menu').attr('lay-id');
					if(layId == 1){
						$('.layui-side').find(".layui-nav-itemed").removeClass('layui-nav-itemed');
					}else if(configObj.theme.leftCloseChild){ // 关闭其他展开的菜单
						var pa = $(this).parent()[0];
						$(pa).siblings().each(function(){
							if($(this).attr('class').indexOf('layui-nav-itemed') != -1){
								var _li_this = this;
								var _a_this = $(_li_this).find('.lay-menu-a')[0];
								var a_child = $(_a_this).next();
								var a_length = a_child.children('dd').length;
								var a_len = a_length * 40;
								a_child.css({
									'overflow': 'hidden',
									'display': 'block',
								});
								a_child.css('height', a_len + 'px');
								var tempLen2 = a_len;
								var tempInter2 = setInterval(function() {
									tempLen2 -= a_length;
									a_child.css('height', tempLen2 + 'px');
									if(tempLen2 <= 0) {
										a_child.find('.layui-nav-itemed').removeClass('layui-nav-itemed');
										$(_li_this).removeClass('layui-nav-itemed');
										clearInterval(tempInter2);
										a_child.attr('style', '');
									}
								}, 1)
							}
						})
					}
				})
			}
		}else{
			// 点击左侧菜单a标签
			$(elem).unbind('click');
			$(elem).on('click', '.lay-menu-a', function() {
				if(configObj.openLeftMenuState){
					return;
				}
				var layChil = $(this).attr('lay-chil');
				if(!layChil){
					return;
				}
				configObj.openLeftMenuState = true;
				var cla = $(this).parent().attr('class');
				var child = $(this).next();
				var length = child.children('dd').length;
				var len = length * 40;
				child.css({
					'overflow': 'hidden',
					'display': 'block',
				});
				var layId = $('.lay-hiden-menu').attr('lay-id');
				if(layId == 1){
					$('.layui-side').find(".layui-nav-itemed").removeClass('layui-nav-itemed');
				}else if(configObj.theme.leftCloseChild){ // 关闭其他展开的菜单
					var pa = $(this).parent()[0];
					$(pa).siblings().each(function(){
						if($(this).attr('class').indexOf('layui-nav-itemed') != -1){
							var _li_this = this;
							var _a_this = $(_li_this).find('.lay-menu-a')[0];
							var a_child = $(_a_this).next();
							var a_length = a_child.children('dd').length;
							var a_len = a_length * 40;
							a_child.css({
								'overflow': 'hidden',
								'display': 'block',
							});
							a_child.css('height', a_len + 'px');
							var tempLen2 = a_len;
							var tempInter2 = setInterval(function() {
								tempLen2 -= a_length;
								a_child.css('height', tempLen2 + 'px');
								if(tempLen2 <= 0) {
									a_child.find('.layui-nav-itemed').removeClass('layui-nav-itemed');
									$(_li_this).removeClass('layui-nav-itemed');
									clearInterval(tempInter2);
									a_child.attr('style', '');
								}
							}, 1)
						}
					})
				}
				if(cla.indexOf('layui-nav-itemed') != -1) {
					child.css('height', '0px');
					var tempLen = 0;
					var tempInter = setInterval(function() {
						tempLen += length;
						child.css('height', tempLen + 'px');
						if(tempLen >= len) {
							clearInterval(tempInter);
							child.attr('style', '');
							configObj.openLeftMenuState = false;
						}
					}, 1)
				} else {
					child.css('height', len + 'px');
					var tempLen = len;
					var tempInter = setInterval(function() {
						tempLen -= length;
						child.css('height', tempLen + 'px');
						if(tempLen <= 0) {
							child.find('.layui-nav-itemed').removeClass('layui-nav-itemed'); // 将子级展开的也收起
							clearInterval(tempInter);
							child.attr('style', '');
							configObj.openLeftMenuState = false;
						}
					}, 1)
				}
			})
		}
		// 移动模式点击遮罩背景关闭菜单
		$(".lay-mobile-bg").unbind('click'); // 防止重复执行点击事件
		$(".lay-mobile-bg").on('click', function(e){
			configObj.hidenMenu('.lay-hiden-menu');
		})
	},
	/** 鼠标放上事件*/
	eventMouseover: function(name, id){
		var layId = $('.lay-hiden-menu').attr('lay-id');
		if(layId == 1){
			configObj.layerIndex = layer.tips(name, '.lay-tips-'+id);
		}
	},
	/** 鼠标移开事件*/
	eventMouseout: function(){
		layer.close(configObj.layerIndex);
	},
	/** 鼠标放上事件 隐藏菜单Dropdown*/
	eventMouseoverDropdown: function(){
		var layId = $('.lay-hiden-menu').attr('lay-id');
		if(!layId){
			$('.lay-dropdown-menu').parents('.layui-dropdown').css('display', 'none');
		}
	},
	/** 监听点击右边标签页-然后选中左边菜单*/
	eventClickTab: function(_this){
		var layId = $('.lay-hiden-menu').attr('lay-id');
		if(layId == 1){
			return;
		}
		if(configObj.theme.leftCloseChild){ // 关闭其他展开的菜单
			if(configObj.layVersion > 268 && configObj.layVersion < 290){
				$('.layui-side').find(".layui-nav-expand").find('.layui-nav-child').attr('style', '');
				$('.layui-side').find(".layui-nav-expand").removeClass("layui-nav-expand");
			}else{
				$('.layui-side').find(".layui-nav-itemed").removeClass('layui-nav-itemed');
			}
		}
		var layId = $(_this).attr('lay-id');
		$('.lay-menu-mark').each(function(){
			var id = $(this).find('span').attr('lay-id');
	    	if(layId == id){
    			$(this).addClass('layui-this');
    			configObj.parentAddItemed($(this).parents(".lay-menu-mark"));
    			if(configObj.layVersion > 268 && configObj.layVersion < 290){
    				$.each($(this).parents(".layui-nav-child"), function(i) {
						$(this).css('display', 'block');
					})
    			}
    		}else{
    			$(this).removeClass('layui-this');
    		}
    	})
	},
	/** 展开左侧指定父级菜单*/
	parentAddItemed: function (obj){
		if(obj){
			if(configObj.layVersion > 268 && configObj.layVersion < 290){
				$.each(obj, function(i) {
					if(!$(this).hasClass("layui-nav-expand")){
						$(this).addClass("layui-nav-expand");
					}
				})
			}else{
				$.each(obj, function(i) {
					if(!$(this).hasClass("layui-nav-itemed")){
						$(this).addClass("layui-nav-itemed");
					}
				})
			}
		}
	},
	/** 选中菜单的展开*/
	openLeftSelectMenu: function(){
		$('.lay-tab>.layui-tab-title li').each(function(){
			var cls = $(this).attr("class")
			var layId = $(this).attr("lay-id");
			if (cls.indexOf('layui-this') != -1) {
				configObj.eventClickTab(this);
			}
		})
	},
	/** 左侧菜单开启时，菜单路径显示*/
	setMenuNavigation: function(names){
		if(names){
			var arr = names.split(","), s = '';
			for(var i=0; i<arr.length; i++){
				s += '<span>'+arr[i]+'</span>';
				if(i < (arr.length-1)){
					s += '<span style="margin: 0px 5px;">/</span>';
				}
			}
			$(".lay-menu-navigation").html('<div class="">'+s+'</div>'); // class="layui-anim lay-anim-left"
		}
	},
	/** 获取对应菜单数据*/
	getMenuData: function (list, id){
		for (var i = 0; i < list.length; i++) {
			var obj = list[i];
			if(obj.mid == id){
				return obj;
			}
			if (obj.children && obj.children.length > 0) {
				var children = configObj.getMenuData(obj.children, id);
				if(children){
					return children;
				}
			}
		}
		return null;
	},
	/** 设置主体标签/内容*/
	setTabUrl: function(obj, _this){
		// 菜单移动端时-选择菜单后关闭左侧菜单
	    var w = $(window).width();
	    if(w < configObj.minWidth){
	    	$('.lay-hiden-menu').attr('lay-id', '');
	    	configObj.hidenMenu('.lay-hiden-menu');
	    }
		var id, url, type, name, icon, headMenuId;
		if(!obj){
			id = $(_this).find('span').attr('lay-id');
			url = $(_this).find('span').attr('lay-url');
			type = $(_this).find('span').attr('lay-type');
			name = $(_this).find('cite').text();
		}else{
			id = obj.mid || obj.id;
			url = obj.url || obj.u;
			name = obj.mname || obj.name;
			type = obj.urlType;
			headMenuId = obj.headMenuId;
			icon = obj.icon;
		}
		if(type && type == 1){
			return window.open(url);
		}
		var tabState = 0;
		$('.lay-tab>.layui-tab-title li').each(function(){
	        var layId = $(this).attr('lay-id');
	        if(layId == id){
	            tabState = 1;
	            if(configObj.theme.page && configObj.theme.refresh){
	            	configObj.refreshPage();
	            }
	            return;
	        }
	    })
		this.swithTab({mid: id, url: url, mname: name, tabState: tabState, headMenuId: headMenuId, icon: icon})
	},
	/** 切换显示标签*/
	swithTab: function(data){
		if(!configObj.theme.page){
			var names = $(".lay-left-menu").find('[lay-id='+data.mid+']').parents('.lay-menu-a').attr('lay-names');
			configObj.setMenuNavigation(names);
			configObj.setTabContent(data);
			return;
		}
		if(!data.tabState){
			if(data.mname.indexOf("lay-icon") == -1){
				if(!data.icon){
					var menuObj = configObj.getMenuData(configObj.menuList, data.mid);
					data.icon = menuObj ? menuObj.icon : '';
				}
				data.titles = data.icon + data.mname;
			}
			var content = '<iframe class="lay-tab-iframe lay-anim '+configObj.animArray[configObj.theme.anim]+'" id="lay-menu-content-'+data.mid+'" name="'+data.mname+'" src="'+data.url+'"></iframe>';
			if(!this.iframe){
				content = '<div class="lay-tab-box lay-anim '+configObj.animArray[configObj.theme.anim]+'" id="lay-menu-content-'+data.mid+'" lay-url="'+data.url+'"></div>';
			}
			element.on('tab('+this.bodyTab+')', function(e){
				var layState = $(this).attr('lay-state');
				var layId = $(this).attr('lay-id');
				if(!layState){
					if(!configObj.iframe){
						$('#lay-menu-content-'+data.mid).load(data.url); // 监听到标签页切换后才加载页面
					}
					$(this).attr('lay-state', true);
					$(this).attr('lay-url', data.url);
					$(this).attr('lay-head-id', data.headMenuId);
					$(this).attr('id', 'lay-tab-title-'+data.mid);
					// 当前内容是否可回到顶部
					if(configObj.scrollTop){
						configObj.eventScrollTop('#lay-menu-content-'+data.mid);
					}
				}
				var headMenuId = $(this).attr('lay-head-id');
				var names = $(".lay-left-menu").find('[lay-id='+layId+']').parents('.lay-menu-a').attr('lay-names');
				configObj.setMenuNavigation(names);
				configObj.showScrollTop('#lay-menu-content-'+layId);
				configObj.setTabTitleScroll(); // 元素滚动到可见区域
				configObj.eventClickTab(this); // 切换左侧菜单选中
				if(headMenuId && configObj.theme.menu == 1){
					$('.lay-head-menu').find('.lay-this').removeClass('lay-this');
					$('#lay-head-'+headMenuId).addClass('lay-this');
				}
			})
			element.tabAdd(this.bodyTab, {
	            title: data.titles,
	            content: content,
	            id: data.mid,
	        })
		}
		element.tabChange(this.bodyTab, data.mid);
	},
	/** 根据主题选择标签页渲染*/
	setTabContent: function(data){
		if(!this.iframe){
			var content = '<div class="lay-tab-box lay-anim '+configObj.animArray[configObj.theme.anim]+'" lay-id="'+data.mid+'" id="lay-menu-content-'+data.mid+'" lay-url="'+data.url+'"></div>';
			$('.lay-tab-content').html(content);
			$('#lay-menu-content-'+data.mid).load(data.url);
		}else{
			var content = '<iframe class="lay-tab-iframe lay-anim '+configObj.animArray[configObj.theme.anim]+'" id="lay-menu-content-'+data.mid+'" name="'+data.mname+'" src="'+data.url+'"></iframe>';
			$('.lay-tab-content').html(content);
		}
		configObj.eventScrollTop('#lay-menu-content-'+data.mid);
	},
	/** 点击左侧-切换，设置右侧标签页选择的标签显示出来*/
	setTabTitleScroll: function(){
		$('.lay-tab>.layui-tab-title').scrollLeft(0);
		var sum = 0, state = true;
		$('.lay-tab>.layui-tab-title li').each(function(){
			var cls = $(this).attr("class")
			if (cls.indexOf('layui-this') != -1) {
				state = false;
			}
			if(state){
				sum += configObj.bodyTabTileWidth;
			}
		})
		var w = $('.lay-tab>.layui-tab-title').width();
		if(w > sum){
			return;
		}
		var avg = sum / w;
		avg = avg < 0 ? 1 : avg;
		var num = w * avg;
		var count = $('.lay-tab>.layui-tab-title').scrollLeft();
		var inter = setInterval(function(){
			if(count < num){
				count = count + configObj.bodyTabTileWidth;
				if(count >= num){
					$('.lay-tab>.layui-tab-title').scrollLeft(num);
					clearInterval(inter);
				}
			}
			if(count >= num){
				clearInterval(inter);
			}
			$('.lay-tab>.layui-tab-title').scrollLeft(count);
		}, 10)
	},
	/** 隐藏/显示菜单*/
	hidenMenu: function(_this){
		var w = $(window).width();
		var layId = $(_this).attr('lay-id');
		if(!layId){
			$(_this).attr('title', '菜单展开');
			$(_this).attr('lay-id', '1');
			$(_this).find('i').html('&#xe66b;');
			if(w <= configObj.minWidth){
				if(configObj.theme.menu == 2){
					$('.lay-head-menu').css('display', 'inline-block');
				}
				if(configObj.theme.logo){
					$('.layui-logo').removeClass('layui-logo-fixed');
				}
				$('.layui-side, .layui-side .layui-nav-tree, .layui-logo').css('width', '0px');
				$('.layui-side, .layui-side .layui-nav-tree, .layui-logo').css('left', '-50px');
				$('.layui-side .layui-nav-tree').css('overflow', 'hidden');
				$('.layui-side').find('.layui-nav-more, cite').css('display', 'none');
				$('.layui-body').css('left', '0px');
				$('.lay-dropdown-div').css('display', 'inline-block');
				$('.lay-mobile-bg').css('display', 'none');
			}else{
				if(!configObj.theme.logo){
					$('.layui-logo').css('width', '55px');
					$('.layui-header .lay-logo-title').css('display', 'none');
				}else{
					$('.layui-logo').addClass('layui-logo-fixed');
				}
				$('.layui-side, .layui-side .layui-nav-tree').css('width', '55px');
				$('.layui-side').find('.layui-nav-more, cite').css('display', 'none');
				$('.layui-body').css('left', '55px');
				$('.layui-side').find(".layui-nav-itemed").removeClass('layui-nav-itemed');
				$('.lay-dropdown-div').css('display', 'inline-block');
				if(configObj.layVersion > 268 && configObj.layVersion < 290){
					$('.layui-side').find(".layui-nav-expand").find('.layui-nav-child').attr('style', '');
					$('.layui-side').find(".layui-nav-expand").removeClass('layui-nav-expand');
					$('.lay-menu-mark').each(function(){
						var cls = $(this).attr('class');
				    	if(cls.indexOf('layui-this') != -1){
		    				$.each($(this).parents(".layui-nav-child"), function(i) {
								$(this).css('display', 'none');
							})
			    		}else{
			    			$(this).removeClass('layui-this');
			    		}
			    	})
				}
			}
			$(".lay-text-div").addClass('lay-text-div-hide');
		}else{
			$(_this).attr('title', '菜单收起');
			$(_this).attr('lay-id', '');
			$(_this).find('i').html('&#xe668;');
			$('.layui-side, .layui-side .layui-nav-tree, .layui-logo').css('left', '0px');
			$('.layui-side .layui-nav-tree').css('overflow', 'unset');
			if(w <= configObj.minWidth){
				if(configObj.theme.menu == 2){
					$('.lay-head-menu').css('display', 'none');
				}
				if(configObj.theme.logo){
					$('.layui-logo').removeClass('layui-logo-fixed');
				}
				$('.layui-side, .layui-side .layui-nav-tree, .layui-logo').css('width', '220px');
				$('.layui-side').find('.layui-nav-more, cite').css('display', 'inline-block');
				//$('.layui-body').css('left', '220px');
				$('.lay-dropdown-div').css('display', 'none');
				$('.lay-mobile-bg').css('display', 'block');
			}else{
				if(!configObj.theme.logo){
					$('.layui-logo').css('width', '220px');
				}else{
					$('.layui-logo').addClass('layui-logo-fixed');
				}
				$('.layui-side, .layui-side .layui-nav-tree').css('width', '220px');
				$('.layui-side').find('.layui-nav-more, cite').css('display', 'inline-block');
				$('.layui-body').css('left', '220px');
				$('.layui-header .lay-logo-title').fadeIn(500);
				$('.lay-dropdown-div').css('display', 'none');
			}
			$(".lay-text-div-hide").removeClass('lay-text-div-hide');
			configObj.openLeftSelectMenu();
		}
	},
	/** 刷新当前页*/
	refreshPage: function(){
		$('.lay-tab>.layui-tab-title li').each(function(){
			var cls = $(this).attr("class")
			var layId = $(this).attr("lay-id");
			if (cls.indexOf('layui-this') != -1) {
				var layUrl = $(this).attr("lay-url");
				if(configObj.iframe){
					document.getElementById('lay-menu-content-'+layId).contentWindow.location.reload(true);
				}else{
					$("#lay-menu-content-"+layId).load(layUrl);
				}
				// 刷新时动画
				/*$("#lay-menu-content-"+layId).removeClass(configObj.animArray[configObj.theme.anim]);
				setTimeout(function(){
					$("#lay-menu-content-"+layId).addClass(configObj.animArray[configObj.theme.anim]);
				}, 100)*/
			}
		})
	},
	/** 主题样式颜色*/
	themeObj: {
		arr1: ['131519','03152A','2a1103','360314',   '131519','03152A','2a1103','360314',   '3b91ff','20a391','e16421','ff6d99',   '3b91ff','20a391','b53332','ff6d99','131519','e16421',    '131519','131519','131519','131519','131519','ffffff']
		,arr2: ['ffffff','ffffff','ffffff','ffffff',   '131519','03152A','2a1103','360314',   '3b91ff','20a391','e16421','ff6d99',   '3b91ff','20a391','b53332','ff6d99','262c38','e16421',    '3b91ff','20a391','b53332','ff6d99','e16421','ffffff']
		,arr3: ['131519','03152A','2a1103','360314',   'ffffff','ffffff','ffffff','ffffff',   'ffffff','ffffff','ffffff','ffffff',   '131519','131519','131519','131519','131519','131519',    '131519','131519','131519','131519','131519','ffffff']
		,inputBorder: ['f9f9f9','f9f9f9','f9f9f9','f9f9f9',   '414142','242938','414142','414142',   '5a98e9','27ab98','e5845e','f189a8',   '5a98e9','27ab98','c76262','f189a8','43444c','e5845e',    '5a98e9','35bba8','c76262','f189a8','e5845e','f9f9f9']
		,headBorder: ['f5f5f5','f5f5f5','f5f5f5','f5f5f5',   '131519','03152A','2a1103','360314',   '3b91ff','20a391','e16421','ff6d99',   '3b91ff','20a391','b53332','ff6d99','262c38','e16421',    '3b91ff','20a391','b53332','ff6d99','e16421','f5f5f5']
		,headColor: ['333333','333333','333333','333333',   'bbbbbb','bbbbbb','bbbbbb','bbbbbb',   'ededed','ededed','ededed','ededed',   'ededed','ededed','ededed','ededed','ededed','ededed',    'ededed','ededed','ededed','ededed','ededed','333333']
		,headHoverColor: ['333333','333333','333333','333333',   'ffffff','ffffff','ffffff','ffffff',   'ffffff','ffffff','ffffff','ffffff',   'ffffff','ffffff','ffffff','ffffff','ffffff','ffffff',    'ffffff','ffffff','ffffff','ffffff','ffffff','333333']
		,headHoverTab: ['131519','03152A','2a1103','360314',   'ffffff','ffffff','ffffff','ffffff',   '86b7f7','35bba8','f59e6f','ffa4bf',   '86b7f7','35bba8','f99494','ffa4bf','35bba8','f59e6f',    '86b7f7','35bba8','f99494','ffa4bf','f59e6f','131519']
		,headMenu: ['00000009','00000009','00000009','00000009',   '383a44','172e4a','48240f','580721',   '5ea1f7','35b3a1','ed7a3c','ff85aa',   '5ea1f7','35b3a1','c94544','ff85aa','363e4e','ed7a3c',    '5ea1f7','35b3a1','c94544','ff85aa','ed7a3c','00000009']
		,logoColor: ['ffffff','ffffff','ffffff','ffffff',   'ffffff','ffffff','ffffff','ffffff',   'ffffff','ffffff','ffffff','ffffff',   'ffffff','ffffff','ffffff','ffffff','ffffff','ffffff',    'ffffff','ffffff','ffffff','ffffff','ffffff','333333']
		,leftColor: ['ffffffcc','ffffffcc','ffffffcc','ffffffcc',   '333333','333333','333333','333333',   '333333','333333','333333','333333',   'ffffffcc','ffffffcc','ffffffcc','ffffffcc','ffffffcc','ffffffcc',    'ffffffcc','ffffffcc','ffffffcc','ffffffcc','ffffffcc','333333']
		,selectBg: ['13b980','3b91ff','dd540a','d71556',   '13b980','3b91ff','dd540a','d71556',   '3b91ff','20a391','e16421','ff6d99',   '3b91ff','20a391','b53332','ff6d99','13b980','e16421',   '3b91ff','20a391','b53332','ff6d99','e16421','00000000']
		,selectColor: ['ffffff','ffffff','ffffff','ffffff',   'ffffff','ffffff','ffffff','ffffff',    'ffffff','ffffff','ffffff','ffffff',    'ffffff','ffffff','ffffff','ffffff','ffffff','ffffff',    'ffffff','ffffff','ffffff','ffffff','ffffff','13b980']
		,childBg: [
			'rgba(0,0,0,.3)','rgba(0,0,0,.3)','rgba(0,0,0,.3)','rgba(0,0,0,.3)',
			'rgb(191 191 191 / 10%)','rgb(191 191 191 / 10%)','rgb(191 191 191 / 10%)','rgb(191 191 191 / 10%)',
			'rgb(191 191 191 / 10%)','rgb(191 191 191 / 10%)','rgb(191 191 191 / 10%)','rgb(191 191 191 / 10%)',
			'rgba(0,0,0,.3)','rgba(0,0,0,.3)','rgba(0,0,0,.3)','rgba(0,0,0,.3)','rgba(0,0,0,.3)','rgba(0,0,0,.3)',
			'rgba(0,0,0,.3)','rgba(0,0,0,.3)','rgba(0,0,0,.3)','rgba(0,0,0,.3)','rgba(0,0,0,.3)','rgb(191 191 191 / 10%)'
		]
	},
	/** 初始化主题*/
	initTheme: function(){
		$('body').append('<style id="lay-theme-style"></style>');
		$('body').append('<style id="lay-theme-style2"></style>');
		$('body').append('<style id="lay-theme-style3"></style>');
		$('body').append('<style id="lay-theme-style4"></style>');
		$('body').append('<style id="lay-theme-style5"></style>');
		$('body').append('<div id="lay-head-theme"></div>');
		if(configObj.theme.leftScroll){
			$("#lay-theme-style5").html(".layui-layout-admin .layui-side-scroll:hover{width: 220px;}");
		}
		if(configObj.theme.page){
			$(".lay-tab").css('display', 'inline-block');
		}else{
			$(".lay-body-box").css('display', 'inline-block');
			$(".lay-body-box").addClass(configObj.animArray[configObj.theme.anim]);
		}
		if(!configObj.theme.pageIcon){
			$('.lay-tab>.layui-tab-title').addClass("lay-icon-hiden");
		}
		var w = $(window).width();
 		if(configObj.theme.logo && w > configObj.minWidth){
			$('.layui-logo').addClass('layui-logo-fixed');
		}else{
			$('.layui-logo').removeClass('layui-logo-fixed');
			$('.layui-logo').css('width', '220px');
		}
		if(configObj.theme.pageType){
			var s1 = '.layui-layout-admin .layui-header{box-shadow: 0 1px 4px rgb(1 11 30 / 8%);border: none;}';
			s1 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title{background: #f3f4f9;padding: 5px 0;height: 30px;box-shadow: none;padding-left: 45px;padding-right: 85px;}';
			s1 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title li{height: 30px;line-height: 30px;background: #fff;color: #444;margin-left: 5px;border-radius: 4px;}';
			s1 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title .layui-this{background: #fff;}';
			s1 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title li:after{display: none;}';
			s1 += '.layui-layout-admin .layui-body .lay-tab>.lay-tab-control .layui-icon{background: #fff;color: #444;top: 5px;height: 30px;line-height: 30px;border-radius: 4px;}';
			s1 += '.layui-layout-admin .layui-body .lay-tab>.lay-tab-control .layui-icon-prev{left: 5px;}';
			s1 += '.layui-layout-admin .layui-body .lay-tab>.lay-tab-control .layui-icon-next{right: 50px;}';
			s1 += '.layui-layout-admin .layui-body .lay-tab>.lay-tab-control .layui-icon-down{right: 5px;}';
			s1 += '.lay-mobile-bg{height: 100%;}';
			$("#lay-theme-style4").html(s1);
		}
		configObj.selectThemes(Number(configObj.theme.theme));
		var s = '';
		s += '<fieldset class="layui-elem-field layui-field-title" style="margin: 10px 0 10px;">';
		s += '	<legend>主题风格</legend>';
		s += '</fieldset>';
		s += '<ul>';
		for(var i=0; i<configObj.themeObj.arr1.length; i++){
			if(configObj.theme.theme == i){
				s += '	<li onclick="configObj.selectThemes('+i+', this)" class="theme-li-this">';
			}else{
				s += '	<li onclick="configObj.selectThemes('+i+', this)" >';
			}
			s += '		<span style="background: #'+configObj.themeObj.arr1[i]+';height:25%;border-bottom: 0px;" class="left_ys"></span>';
			s += '		<span style="background: #'+configObj.themeObj.arr2[i]+';" class="right_ys"></span>';
			s += '		<span style="background: #'+configObj.themeObj.arr3[i]+';height:75%;border-right: 1px solid #f5efef;" class="left_ys"></span>';
			s += '	</li>';
		}
		s += '</ul>';

		s += '<fieldset class="layui-elem-field layui-field-title" style="margin: 10px 0 10px;">';
		s += '	<legend>菜单类型</legend>';
		s += '</fieldset>';
		s += '<div class="layui-form">'
		s += '<div class="switch-box">';
		s += '	<span class="switch-text-left">左侧菜单</span>';
		s += '	<span class="switch-text-right">';
		s += '		<input type="radio" '+(!configObj.theme.menu || configObj.theme.menu == 0?'checked':'')+' name="lay_menu_type" lay-filter="lay_menu_type" value="0" title="">';
		s += '	</span>';
		s += '</div>';
		s += '<div class="switch-box">';
		s += '	<span class="switch-text-left">头部菜单</span>';
		s += '	<span class="switch-text-right">';
		s += '		<input type="radio" '+(configObj.theme.menu == 1?'checked':'')+' name="lay_menu_type" lay-filter="lay_menu_type" value="1" title="">';
		s += '	</span>';
		s += '</div>';
		s += '<div class="switch-box">';
		s += '	<span class="switch-text-left">混合菜单</span>';
		s += '	<span class="switch-text-right">';
		s += '		<input type="radio" '+(configObj.theme.menu == 2?'checked':'')+' name="lay_menu_type" lay-filter="lay_menu_type" value="2" title="">';
		s += '	</span>';
		s += '</div>';
		s += '<fieldset class="layui-elem-field layui-field-title" style="margin: 10px 0 10px;">';
		s += '	<legend>标签页</legend>';
		s += '</fieldset>';
		s += '<div class="switch-box">';
		s += '	<span class="switch-text-left">标签页开启</span>';
		s += '	<span class="switch-text-right">';
		s += '		<input type="checkbox" '+(configObj.theme.page?'checked':'')+' name="lay_theme_page" lay-skin="switch" lay-filter="lay_theme_page" lay-text="ON|OFF">';
		s += '	</span>';
		s += '</div>';
		s += '<div class="switch-box">';
		s += '	<span class="switch-text-left">标签页图标</span>';
		s += '	<span class="switch-text-right">';
		s += '		<input type="checkbox" '+(configObj.theme.pageIcon?'checked':'')+' name="lay_theme_page_icon" lay-skin="switch" lay-filter="lay_theme_page_icon" lay-text="ON|OFF">';
		s += '	</span>';
		s += '</div>';
		s += '<div class="switch-box">';
		s += '	<span class="switch-text-left">标签页圆角效果</span>';
		s += '	<span class="switch-text-right">';
		s += '		<input type="checkbox" '+(configObj.theme.pageType?'checked':'')+' name="lay_theme_page_type" lay-skin="switch" lay-filter="lay_theme_page_type" lay-text="ON|OFF">';
		s += '	</span>';
		s += '</div>';
		s += '<fieldset class="layui-elem-field layui-field-title" style="margin: 10px 0 10px;">';
		s += '	<legend>菜单配置</legend>';
		s += '</fieldset>';
		s += '<div class="switch-box">';
		s += '	<span class="switch-text-left">点击菜单刷新</span>';
		s += '	<span class="switch-text-right">';
		s += '		<input type="checkbox" '+(configObj.theme.refresh?'checked':'')+' name="lay_theme_lefts" lay-skin="switch" lay-filter="lay_theme_lefts" lay-text="ON|OFF">';
		s += '	</span>';
		s += '</div>';
		s += '<div class="switch-box">';
		s += '	<span class="switch-text-left">菜单选中圆角</span>';
		s += '	<span class="switch-text-right">';
		s += '		<input type="checkbox" '+(configObj.theme.radius?'checked':'')+' name="lay_select_menu" lay-skin="switch" lay-filter="lay_select_menu" lay-text="ON|OFF">';
		s += '	</span>';
		s += '</div>';
		s += '<div class="switch-box">';
		s += '	<span class="switch-text-left">左侧菜单滚动条</span>';
		s += '	<span class="switch-text-right">';
		s += '		<input type="checkbox" '+(configObj.theme.leftScroll?'checked':'')+' name="lay_leftScroll" lay-skin="switch" lay-filter="lay_leftScroll" lay-text="ON|OFF">';
		s += '	</span>';
		s += '</div>';
		s += '<div class="switch-box">';
		s += '	<span class="switch-text-left">关闭其他展开的子菜单</span>';
		s += '	<span class="switch-text-right">';
		s += '		<input type="checkbox" '+(configObj.theme.leftCloseChild?'checked':'')+' name="lay_leftCloseChild" lay-skin="switch" lay-filter="lay_leftCloseChild" lay-text="ON|OFF">';
		s += '	</span>';
		s += '</div>';
		s += '<fieldset class="layui-elem-field layui-field-title" style="margin: 10px 0 10px;">';
		s += '	<legend>其他</legend>';
		s += '</fieldset>';
		s += '<div class="switch-box">';
		s += '	<span class="switch-text-left">logo固定</span>';
		s += '	<span class="switch-text-right">';
		s += '		<input type="checkbox" '+(configObj.theme.logo?'checked':'')+' name="lay_theme_logo" lay-skin="switch" lay-filter="lay_theme_logo" lay-text="ON|OFF">';
		s += '	</span>';
		s += '</div>';
		s += '<fieldset class="layui-elem-field layui-field-title" style="margin: 10px 0 10px;">';
		s += '	<legend>页面动画效果</legend>';
		s += '</fieldset>';
		s += '<div class="switch-box">';
		s += '		<select name="lay_theme_anim" lay-filter="lay_theme_anim">';
		s += '			<option value="0" '+(!configObj.theme.anim || configObj.theme.anim == 0?'selected':'')+'>无</option>';
		s += '			<option value="1" '+(configObj.theme.anim == 1?'selected':'')+'>渐渐显示</option>';
		s += '			<option value="2" '+(configObj.theme.anim == 2?'selected':'')+'>从顶部渐渐显示</option>';
		s += '			<option value="3" '+(configObj.theme.anim == 3?'selected':'')+'>从底部渐渐显示</option>';
		s += '			<option value="4" '+(configObj.theme.anim == 4?'selected':'')+'>从左侧渐渐显示</option>';
		s += '			<option value="5" '+(configObj.theme.anim == 5?'selected':'')+'>从右侧渐渐显示</option>';
		s += '		</select>';
		s += '</div>';
		s += '</div>';
		$('#lay-head-theme').html(s);
		form.render();
		// 监听菜单类型
		form.on('radio(lay_menu_type)', function(data){
			var index = layMin.load({elem: "#lay-head-theme", bg: ['white', 0.2], type: 1});
 			configObj.theme.menu = data.value;
 			configObj.loadMenu(configObj.menuList);
			if(configObj.themeCallback){
				configObj.themeCallback(configObj.theme);
			}
			layMin.close(index, 10);
        });
        // 监听logo固定
		form.on('switch(lay_theme_logo)', function(data){
 			configObj.theme.logo = data.elem.checked;
 			var w = $(window).width();
 			if(configObj.theme.logo && w > configObj.minWidth){
 				$('.layui-logo').addClass('layui-logo-fixed');
			}else{
				$('.layui-logo').removeClass('layui-logo-fixed');
				$('.layui-logo').css('width', '220px');
			}
 			if(configObj.themeCallback){
				configObj.themeCallback(configObj.theme);
			}
        });
		// 监听标签页
		form.on('switch(lay_theme_page)', function(data){
 			configObj.theme.page = data.elem.checked;
 			if(configObj.theme.page){
				$(".lay-tab").css('display', 'inline-block');
				$(".lay-body-box").css('display', 'none');
			}else{
				$(".lay-tab").css('display', 'none');
				$(".lay-body-box").css('display', 'inline-block');
				$('.lay-tab>.layui-tab-title li').each(function(){
			        var layId = $(this).attr('lay-id');
			        element.tabDelete(configObj.bodyTab, layId);
			    })
			}
			$('.lay-tab-content').empty();
			$('.lay-menu-mark').each(function(){
				var cls = $(this).attr('class');
				if(cls.indexOf('layui-this') != -1){
					configObj.setTabUrl(null, this);
				}
	    	})
			if(configObj.themeCallback){
				configObj.themeCallback(configObj.theme);
			}
        });
        // 监听标签页图标
		form.on('switch(lay_theme_page_icon)', function(data){
 			configObj.theme.pageIcon = data.elem.checked;
 			if(configObj.theme.pageIcon){
				$('.lay-tab>.layui-tab-title').removeClass("lay-icon-hiden");
			}else{
				$('.lay-tab>.layui-tab-title').addClass("lay-icon-hiden");
			}
			if(configObj.themeCallback){
				configObj.themeCallback(configObj.theme);
			}
        });
        // 监听标签页圆角
		form.on('switch(lay_theme_page_type)', function(data){
 			configObj.theme.pageType = data.elem.checked;
 			if(configObj.theme.pageType){
 				var s1 = '.layui-layout-admin .layui-header{box-shadow: 0 1px 4px rgb(1 11 30 / 8%);border: none;}';
 				s1 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title{background: #f3f4f9;padding: 5px 0;height: 30px;box-shadow: none;padding-left: 45px;padding-right: 85px;}';
 				s1 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title li{height: 30px;line-height: 30px;background: #fff;color: #444;margin-left: 5px;border-radius: 4px;}';
 				s1 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title .layui-this{background: #fff;}';
 				s1 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title li:after{display: none;}';
 				s1 += '.layui-layout-admin .layui-body .lay-tab>.lay-tab-control .layui-icon{background: #fff;color: #444;top: 5px;height: 30px;line-height: 30px;border-radius: 4px;}';
 				s1 += '.layui-layout-admin .layui-body .lay-tab>.lay-tab-control .layui-icon-prev{left: 5px;}';
 				s1 += '.layui-layout-admin .layui-body .lay-tab>.lay-tab-control .layui-icon-next{right: 50px;}';
 				s1 += '.layui-layout-admin .layui-body .lay-tab>.lay-tab-control .layui-icon-down{right: 5px;}';
 				s1 += '.lay-mobile-bg{height: 100%;}';
				$("#lay-theme-style4").html(s1);
			}else{
				$("#lay-theme-style4").empty();
			}
			if(configObj.themeCallback){
				configObj.themeCallback(configObj.theme);
			}
        });
        // 监听点击菜单刷新
		form.on('switch(lay_theme_lefts)', function(data){
 			configObj.theme.refresh = data.elem.checked;
 			if(configObj.themeCallback){
				configObj.themeCallback(configObj.theme);
			}
        });
        // 监听左侧菜单圆角
		form.on('switch(lay_select_menu)', function(data){
			configObj.theme.radius = data.elem.checked;
			var i = configObj.theme.theme;
			// 左侧菜单选择圆角
			if(configObj.theme.radius && i != 23){
				var s1 = '', s2 = '';
				s1 += '.layui-side .layui-nav .layui-nav-item a:hover,';
				s2 += '.layui-side .layui-nav-tree .layui-nav-child dd.layui-this,';
				s2 += '.layui-side .layui-nav-tree .layui-nav-child dd.layui-this a,';
				s2 += '.layui-side .layui-nav-tree .layui-this,';
				s2 += '.layui-side .layui-nav-tree .layui-this a,';
				s2 += '.layui-side .layui-nav-tree .layui-this a:hover,';
				s2 += '.layui-side .layui-nav-tree .layui-nav-bar {background-color: #00000000!important;color: #ffffff!important;}';
				if(i <= 11 && i >= 4){
					s1 += '.layui-side .layui-nav .layui-this a {color: #'+configObj.themeObj.selectBg[i]+'!important;}';
					s2 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this .lay-text-div span{color: #'+configObj.themeObj.selectBg[i]+'!important;}';
					s2 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this a span:hover{color: #'+configObj.themeObj.selectBg[i]+'!important;}';
					s2 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this .lay-text-div{background-color: #'+configObj.themeObj.selectBg[i]+'24;color: #'+configObj.themeObj.selectBg[i]+';padding-left: 10px;}';
					s2 += '.layui-layout-admin .layui-side-scroll::-webkit-scrollbar-thumb {background-color:#c1c1c1;border-radius: 20px;display: none;}'
				}else{
					s1 += '.layui-side .layui-nav .layui-this a {color: #'+configObj.themeObj.leftColor[i]+'!important;}';
					s2 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this .lay-text-div{background-color: #'+configObj.themeObj.selectBg[i]+';color: #'+configObj.themeObj.selectColor[i]+';padding-left: 10px;}';
				}
				$("#lay-theme-style2").html(s1 + s2);
			}else{
				var s1 = '';
				if(i <= 11 && i >= 4){
					s1 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this,';
					s1 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this a,';
					s1 += '.layui-side .layui-nav-tree .layui-nav-child dd.layui-this,';
					s1 += '.layui-side .layui-nav-tree .layui-nav-child dd.layui-this a,';
					s1 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-nav-bar {background-color: #'+configObj.themeObj.selectBg[i]+'10!important;color: #'+configObj.themeObj.selectColor[i]+'!important;}';
					s1 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this a span{color: #'+configObj.themeObj.selectBg[i]+'!important;}';
					s1 += '.layui-layout-admin .layui-side .layui-nav-item a{transition: unset;}';
					s1 += '.layui-layout-admin .layui-nav-tree .layui-this>a {border-right: 2px solid #'+configObj.themeObj.selectBg[i]+';}';
				}
				$("#lay-theme-style2").html(s1);
			}
			// 头部选中效果-圆角
			var s3 = '';
			if(configObj.theme.radius){
				s3 += '.layui-layout-admin .layui-header .lay-head-menu .lay-this span{background-color: #'+configObj.themeObj.headMenu[i]+';color: #'+configObj.themeObj.headColor[i]+';}';
				s3 += '.layui-layout-admin .layui-header .lay-head-menu>li:hover span{background: #'+configObj.themeObj.headMenu[i]+';}';
			}else{
				s3 += '.layui-layout-admin .layui-header .lay-head-menu .lay-this{background: #'+configObj.themeObj.headMenu[i]+';}';
				s3 += '.layui-layout-admin .layui-header .lay-head-menu>li:hover{background: #'+configObj.themeObj.headMenu[i]+';}';
			}
			s3 += '.layui-layout-admin .layui-header .lay-head-menu>li cite,.layui-layout-admin .layui-header .lay-head-menu>li a{color: #'+configObj.themeObj.headColor[i]+';}';
			$("#lay-theme-style3").html(s3);
 			if(configObj.themeCallback){
				configObj.themeCallback(configObj.theme);
			}
        });
		// 监听菜单滚动条
		form.on('switch(lay_leftScroll)', function(data){
			configObj.theme.leftScroll = data.elem.checked;
			if(configObj.themeCallback){
				configObj.themeCallback(configObj.theme);
			}
			$("#lay-theme-style5").html(configObj.theme.leftScroll ? ".layui-layout-admin .layui-side-scroll:hover{width: 220px;}" : "");
		});
		// 监听关闭其他展开的菜单
		form.on('switch(lay_leftCloseChild)', function(data){
			configObj.theme.leftCloseChild = data.elem.checked;
			if(configObj.themeCallback){
				configObj.themeCallback(configObj.theme);
			}
		});
        // 监听改变动画效果
		form.on('select(lay_theme_anim)', function(data){
			$("."+configObj.animArray[configObj.theme.anim]).addClass(configObj.animArray[Number(data.value)]);
			$("."+configObj.animArray[configObj.theme.anim]).removeClass(configObj.animArray[configObj.theme.anim]);
 			configObj.theme.anim = Number(data.value);
 			if(configObj.themeCallback){
				configObj.themeCallback(configObj.theme);
			}
        });
	},
	/** 选择主题*/
	selectThemes: function(i, _this){
		// 头部
		var s1 = '.layui-layout-admin .layui-header{background-color: #'+configObj.themeObj.arr2[i]+'!important;border-bottom: 1px solid #'+configObj.themeObj.headBorder[i]+';}';
		s1 += '.layui-layout-admin .layui-header .lay-tree-menu input{border-color: #'+configObj.themeObj.inputBorder[i]+'!important;color: #'+configObj.themeObj.headColor[i]+';}';
		s1 += '.layui-layout-admin .layui-header .lay-tree-menu input::placeholder{color: #'+configObj.themeObj.headColor[i]+'!important;}';
		s1 += '.layui-layout-admin .layui-header .layui-nav-item>a, .layui-layout-admin .layui-header .layui-nav-item>a cite{color: #'+configObj.themeObj.headColor[i]+';}';
		s1 += '.layui-layout-admin .layui-header .layui-nav-item>a:hover{color: #'+configObj.themeObj.headHoverColor[i]+';}';
		s1 += '.layui-layout-admin .layui-header .layui-nav-bar{background-color: #'+configObj.themeObj.headHoverTab[i]+'!important;}';
		s1 += '.layui-layout-admin .layui-header .lay-menu-navigation{color: #'+configObj.themeObj.headColor[i]+';}';
		// logo
		var s2 = '.layui-layout-admin .layui-logo{background-color: #'+configObj.themeObj.arr1[i]+'!important;color: #'+configObj.themeObj.logoColor[i]+'!important;}';
		// 左侧
		var s3 = '.layui-layout-admin .layui-side,';
		s3 += '.layui-layout-admin .layui-side .layui-nav{background-color: #'+configObj.themeObj.arr3[i]+'!important;color: #'+configObj.themeObj.leftColor[i]+'!important;}';
		s3 += '.layui-side .layui-nav-tree .layui-nav-item a{color: #'+configObj.themeObj.leftColor[i]+'!important;}';
		s3 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-nav-child {background-color: '+configObj.themeObj.childBg[i]+'!important;}';
		s3 += '.layui-side .layui-nav .layui-nav-item a:hover,';
		s3 += '.layui-side .layui-nav .layui-this a {color: #'+configObj.themeObj.leftColor[i]+'!important;}';
		if(!configObj.theme.radius && i <= 11 && i >= 4){
			s3 += '.layui-side .layui-nav-tree .layui-this,';
			s3 += '.layui-side .layui-nav-tree .layui-this a,';
			s3 += '.layui-side .layui-nav-tree .layui-nav-bar {background-color: #'+configObj.themeObj.selectBg[i]+'10!important;color: #'+configObj.themeObj.selectColor[i]+'!important;}';
			s3 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this a span{color: #'+configObj.themeObj.selectBg[i]+'!important;}';
		}else{
			s3 += '.layui-side .layui-nav-tree .layui-nav-child dd.layui-this,';
			s3 += '.layui-side .layui-nav-tree .layui-nav-child dd.layui-this a,';
			s3 += '.layui-side .layui-nav-tree .layui-this,';
			s3 += '.layui-side .layui-nav-tree .layui-this a,';
			s3 += '.layui-side .layui-nav-tree .layui-this a:hover,';
			s3 += '.layui-side .layui-nav-tree .layui-nav-bar {background-color: #'+configObj.themeObj.selectBg[i]+'!important;color: #'+configObj.themeObj.selectColor[i]+'!important;}';
			s3 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this a span{color: #'+configObj.themeObj.selectColor[i]+'!important;}';
		}
		if(i == 23){
			s3 += '.layui-layout-admin .layui-side-scroll::-webkit-scrollbar-thumb {background-color:#c1c1c1;border-radius: 20px;display: none;}'
			s3 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title li:after{background-color: #'+configObj.themeObj.selectColor[i]+'!important}';
			s3 += '.layui-layout-admin .layui-body .layui-tab-brief>.layui-tab-title .layui-this{color: #'+configObj.themeObj.selectColor[i]+'!important;}';
		}else{
			s3 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title li:after{background-color: #'+configObj.themeObj.selectBg[i]+'!important}';
			s3 += '.layui-layout-admin .layui-body .layui-tab-brief>.layui-tab-title .layui-this{color: #'+configObj.themeObj.selectBg[i]+'!important;}';
			s3 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title li:hover{color: #'+configObj.themeObj.selectBg[i]+'!important;}';
		}
		if((i <= 3 && i >= 0) || i == 23){
			s3 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title li:after{top: 0;bottom: unset;}';
			s3 += '.layui-layout-admin .layui-header .lay-head-menu>.lay-this a,.layui-layout-admin .layui-header .lay-head-menu>.lay-this cite{color: #111111;}';
		}else if(i >= 4){
			s3 += '.layui-layout-admin .layui-body .lay-tab>.layui-tab-title li:after{top: unset;bottom: 0;}';
			s3 += '.layui-layout-admin .layui-header .lay-head-menu>.lay-this a,.layui-layout-admin .layui-header .lay-head-menu>.lay-this cite{color: #fcfcfc;}'
		}
		$("#lay-theme-style").html(s1 + s2 + s3);
		// 左侧菜单选择圆角
		s3 = '';
		s3 += '.layui-side .layui-nav .layui-nav-item a:hover,';
		if(configObj.theme.radius && i != 23){
			if(i <= 11 && i >= 4){
				s3 += '.layui-side .layui-nav .layui-this a {color: #'+configObj.themeObj.selectBg[i]+'!important;}';
				s3 += '.layui-side .layui-nav-tree .layui-this a span:hover{color: #'+configObj.themeObj.selectBg[i]+'!important;}';
			}else{
				s3 += '.layui-side .layui-nav .layui-this a {color: #'+configObj.themeObj.leftColor[i]+'!important;}';
			}
		}else{
			if(i <= 11 && i >= 4){
				s3 += '.layui-layout-admin .layui-side .layui-nav-item a{transition: unset;}';
				s3 += '.layui-nav-tree .layui-this>a {border-right: 2px solid #'+configObj.themeObj.selectBg[i]+';}';
			}
		}
		s3 += '.layui-side .layui-nav-tree .layui-nav-child dd.layui-this,';
		s3 += '.layui-side .layui-nav-tree .layui-nav-child dd.layui-this a,';
		s3 += '.layui-side .layui-nav-tree .layui-this,';
		s3 += '.layui-side .layui-nav-tree .layui-this a,';
		s3 += '.layui-side .layui-nav-tree .layui-this a:hover,';
		if(configObj.theme.radius && i != 23){
			s3 += '.layui-side .layui-nav-tree .layui-nav-bar {background-color: #00000000!important;color: #ffffff!important;}';
			if(i <= 11 && i >= 4){
				s3 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this .lay-text-div span{color: #'+configObj.themeObj.selectBg[i]+'!important;}';
				s3 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this .lay-text-div{background-color: #'+configObj.themeObj.selectBg[i]+'24;color: #'+configObj.themeObj.selectBg[i]+';padding-left: 10px;}';
				s3 += '.layui-layout-admin .layui-side-scroll::-webkit-scrollbar-thumb {background-color:#c1c1c1;border-radius: 20px;display: none;}'
			}else{
				s3 += '.layui-layout-admin .layui-side .layui-nav-tree .layui-this .lay-text-div{background-color: #'+configObj.themeObj.selectBg[i]+';color: #'+configObj.themeObj.selectColor[i]+';padding-left: 10px;}';
			}
		}
		$("#lay-theme-style2").html(s3);
		// 头部选中效果-圆角
		s3 = '';
		if(configObj.theme.radius){
			s3 += '.layui-layout-admin .layui-header .lay-head-menu .lay-this span{background-color: #'+configObj.themeObj.headMenu[i]+';color: #'+configObj.themeObj.headColor[i]+';}';
			s3 += '.layui-layout-admin .layui-header .lay-head-menu>li:hover span{background: #'+configObj.themeObj.headMenu[i]+';}';
		}else{
			s3 += '.layui-layout-admin .layui-header .lay-head-menu .lay-this{background: #'+configObj.themeObj.headMenu[i]+';}';
			s3 += '.layui-layout-admin .layui-header .lay-head-menu>li:hover{background: #'+configObj.themeObj.headMenu[i]+';}';
		}
		s3 += '.layui-layout-admin .layui-header .lay-head-menu>li cite,.layui-layout-admin .layui-header .lay-head-menu>li a{color: #'+configObj.themeObj.headColor[i]+';}';
		$("#lay-theme-style3").html(s3);

		$('.theme-li-this').removeClass('theme-li-this');
		if(_this){
			$(_this).addClass('theme-li-this');
		}
		configObj.theme.theme = i;
		if(configObj.themeCallback){
			configObj.themeCallback(configObj.theme);
		}
	},
	/** 更换主题*/
	openSettingTheme: function(){
		layMin.openLayer({
			elem: "body", // 在哪个容器弹出，默认body
			type: 2, // 弹框类型：1、内容，2 、html元素标签 3、iframe页面
			title: false, // 弹框标题，可设置false
			colseIcon: false, // 是否隐藏关闭按钮
			shade: 0.5, // 背景透明度0 ~ 1，可设置false
			shadeClose: true, // 点击遮罩关闭
			width: '280', // 弹框宽度
			height: '100%', // 弹框高度
			offset: {
				'right': '0px',
				'bottom': '0px',
				'left': 'unset',
				'top': 'unset'
			}, // 弹出位置，unset代表取消样式
			anim: 3, // 动画 0 ~ 5 ，可设置false
			btns: false,
			content: '#lay-head-theme', // 内容
			success: function(index, elem) {
				$(elem).css('border-radius', '0px');
			},
		})
	},
	/** 全屏/退出全屏*/
	fullOrExitScreen: function(_this){
		var id = $(_this).attr('lay-id');
		if(id == 0){
			this.fullScreen();
			$(_this).attr({'lay-id': "1", "title": "退出全屏"});
			$(_this).find(".layui-icon").html('&#xe758;');
		}else{
			this.exitFullscreen();
			$(_this).attr({'lay-id': "0", "title": "全屏"});
			$(_this).find(".layui-icon").html('&#xe622;');
		}
	},
	/** 全屏*/
	fullScreen: function(){
		var element = document.documentElement;
	    if (element.requestFullscreen) {
	        element.requestFullscreen();
	    } else if (element.msRequestFullscreen) {
	        element.msRequestFullscreen();
	    } else if (element.mozRequestFullScreen) {
	        element.mozRequestFullScreen();
	    } else if (element.webkitRequestFullscreen) {
	        element.webkitRequestFullscreen();
	    }
	},
	/** 退出全屏*/
	exitFullscreen: function(){
		if (document.exitFullscreen) {
	        document.exitFullscreen();
	    } else if (document.msExitFullscreen) {
	        document.msExitFullscreen();
	    } else if (document.mozCancelFullScreen) {
	        document.mozCancelFullScreen();
	    } else if (document.webkitExitFullscreen) {
	        document.webkitExitFullscreen();
	    }
	},
	/** 回到顶部*/
	scrollTopLabel: function(){
		if(this.scrollTopState){
			return;
		}
		var s = '<div class="lay-scrollTop" title="回到顶部">';
		s += '	<i class="layui-icon layui-icon-up"></i>';
		s += '</div>';
		$('body').append(s);
		this.scrollTopState = true;
	},
	/** 监听回到顶部*/
	eventScrollTop: function(label){
		if(this.iframe){
			var frameWidow = document.getElementById(label.replace('#','')).contentWindow;
			//监听
			frameWidow.onscroll = function() {
				configObj.showScrollTop(label);
				$(".lay-scrollTop").unbind('click');
				$(".lay-scrollTop").click(function(){
					if(configObj.theme.menu == 1){
						if(configObj.theme.page){
							$('.lay-tab>.layui-tab-title li').each(function(){
								var layId = $(this).attr("lay-id");
								var frameWidow = document.getElementById('lay-menu-content-'+layId).contentWindow;
								var num = frameWidow.document.documentElement.scrollTop
								|| frameWidow.document.body.scrollTop
								|| frameWidow.pageYOffset
								|| frameWidow.document.scrollingElement.scrollTop;
								var time = 10 / num;
								var layInter = setInterval(function(){
									num -= 30;
									if(num <= 0){
										clearInterval(layInter);
									}
									frameWidow.document.documentElement.scrollTop = num;
									frameWidow.document.body.scrollTop = num;
									frameWidow.pageYOffset = num;
									frameWidow.document.scrollingElement.scrollTop = num;
								}, time)
							})
						}else{
							var layId = configObj.menuLastMid;
							var frameWidow = document.getElementById('lay-menu-content-'+layId).contentWindow;
							var num = frameWidow.document.documentElement.scrollTop
							|| frameWidow.document.body.scrollTop
							|| frameWidow.pageYOffset
							|| frameWidow.document.scrollingElement.scrollTop;
							var time = 10 / num;
							var layInter = setInterval(function(){
								num -= 30;
								if(num <= 0){
									clearInterval(layInter);
								}
								frameWidow.document.documentElement.scrollTop = num;
								frameWidow.document.body.scrollTop = num;
								frameWidow.pageYOffset = num;
								frameWidow.document.scrollingElement.scrollTop = num;
							}, time)
						}
					}else if(configObj.theme.page){
						$('.lay-tab>.layui-tab-title li').each(function(){
							var cls = $(this).attr("class")
							if (cls.indexOf('layui-this') != -1) {
								var layId = $(this).attr("lay-id");
								var frameWidow = document.getElementById('lay-menu-content-'+layId).contentWindow;
								var num = frameWidow.document.documentElement.scrollTop
								|| frameWidow.document.body.scrollTop
								|| frameWidow.pageYOffset
								|| frameWidow.document.scrollingElement.scrollTop;
								var time = 10 / num;
								var layInter = setInterval(function(){
									num -= 30;
									if(num <= 0){
										clearInterval(layInter);
									}
									frameWidow.document.documentElement.scrollTop = num;
									frameWidow.document.body.scrollTop = num;
									frameWidow.pageYOffset = num;
									frameWidow.document.scrollingElement.scrollTop = num;
								}, time)
							}
						})
					}else{
						$('.lay-menu-mark').each(function(){
							var cls = $(this).attr("class")
							if (cls.indexOf('layui-this') != -1) {
								var layId = $(this).find("span").attr("lay-id");
								var frameWidow = document.getElementById('lay-menu-content-'+layId).contentWindow;
								var num = frameWidow.document.documentElement.scrollTop
								|| frameWidow.document.body.scrollTop
								|| frameWidow.pageYOffset
								|| frameWidow.document.scrollingElement.scrollTop;
								var time = 10 / num;
								var layInter = setInterval(function(){
									num -= 30;
									if(num <= 0){
										clearInterval(layInter);
									}
									frameWidow.document.documentElement.scrollTop = num;
									frameWidow.document.body.scrollTop = num;
									frameWidow.pageYOffset = num;
									frameWidow.document.scrollingElement.scrollTop = num;
								}, time)
							}
						})
					}
				})
			};
		}else{
			// 获取容器对象
			var container = $(label);
			// 绑定滚动事件处理函数
			container.scroll(function() {
				configObj.showScrollTop(label);
				$(".lay-scrollTop").unbind('click');
				$(".lay-scrollTop").click(function(){
					if(configObj.theme.menu == 1){
						if(configObj.theme.page){
							$('.lay-tab>.layui-tab-title li').each(function(){
								var layId = $(this).attr("lay-id");
								var num = $('#lay-menu-content-'+layId).scrollTop();
								var time = 10 / num;
								var layInter = setInterval(function(){
									num -= 30;
									$('#lay-menu-content-'+layId).scrollTop(num);
									if(num <= 0){
										clearInterval(layInter);
									}
								}, time)
							})
						}else{
							var layId = configObj.menuLastMid;
							var num = $('#lay-menu-content-'+layId).scrollTop();
							var time = 10 / num;
							var layInter = setInterval(function(){
								num -= 30;
								$('#lay-menu-content-'+layId).scrollTop(num);
								if(num <= 0){
									clearInterval(layInter);
								}
							}, time)
						}
					}else if(configObj.theme.page){
						$('.lay-tab>.layui-tab-title li').each(function(){
							var cls = $(this).attr("class")
							if (cls.indexOf('layui-this') != -1) {
								var layId = $(this).attr("lay-id");
								var num = $('#lay-menu-content-'+layId).scrollTop();
								var time = 10 / num;
								var layInter = setInterval(function(){
									num -= 30;
									$('#lay-menu-content-'+layId).scrollTop(num);
									if(num <= 0){
										clearInterval(layInter);
									}
								}, time)
							}
						})
					}else{
						$('.lay-menu-mark').each(function(){
							var cls = $(this).attr("class")
							if (cls.indexOf('layui-this') != -1) {
								var layId = $(this).find("span").attr("lay-id");
								var num = $('#lay-menu-content-'+layId).scrollTop();
								var time = 10 / num;
								var layInter = setInterval(function(){
									num -= 30;
									$('#lay-menu-content-'+layId).scrollTop(num);
									if(num <= 0){
										clearInterval(layInter);
									}
								}, time)
							}
						})
					}
				})
			});
		}
	},
	/** 显示回到顶部*/
	showScrollTop: function(label){
		if(this.iframe){
			var frameWidow = document.getElementById(label.replace('#','')).contentWindow;
			//获取滚动条的位置
			var num = frameWidow.document.documentElement.scrollTop
			|| frameWidow.document.body.scrollTop
			|| frameWidow.pageYOffset
			|| frameWidow.document.scrollingElement.scrollTop;
			if(num >= 500) {
				$(".lay-scrollTop").css('display', 'inline-block');
			}else{
				$(".lay-scrollTop").css('display', 'none');
			}
		}else{
			var num = $(label).scrollTop();
			if(num >= 500) {
				$(".lay-scrollTop").css('display', 'inline-block');
			}else{
				$(".lay-scrollTop").css('display', 'none');
			}
		}
	},
	/** 自适应*/
	resize: function(){
		$(window).resize(function(){
			var w = $(window).width();
			if(configObj.theme.menu != 1){
				// 菜单设置为移动端
			    if(w < configObj.minWidth){
			    	$('.lay-hiden-menu').attr('lay-id', '');
			    }else{
			    	$('.lay-hiden-menu').attr('lay-id', 1);
			    }
			    configObj.hidenMenu('.lay-hiden-menu');
			}else{
		    	if(w < configObj.minWidth){
					if(configObj.theme.logo){
						$('.layui-logo').removeClass('layui-logo-fixed');
					}
					$('.layui-logo').css({'width': '0px' , 'left': '-50px'});
				}else{
					if(configObj.theme.logo){
						$('.layui-logo').addClass('layui-logo-fixed');
					}else{
						$('.layui-logo').css({'width': '220px' , 'left': '0px'});
					}
				}
			}
		    // 监听window是否全屏，并进行相应的操作,支持esc键退出
		    var isFull=!!(document.webkitIsFullScreen || document.mozFullScreen ||
		        document.msFullscreenElement || document.fullscreenElement
		    );//!document.webkitIsFullScreen都为true。因此用!!
		    if (isFull == false) {
				$(".lay-screen").attr({'lay-id': "0", "title": "全屏"});
				$(".lay-screen i").html('&#xe622;');
			}else{
				$(".lay-screen").attr({'lay-id': "1", "title": "退出全屏"});
				$(".lay-screen i").html('&#xe758;');
			}
			if(configObj.theme.menu == 1 || configObj.theme.menu == 2){
				if(configObj.sizeTimeout){
					clearTimeout(configObj.sizeTimeout);
				}
				configObj.sizeTimeout = setTimeout(function(){
					if(configObj.theme.menu == 1){
						configObj.maxHeadMenu();
					}else{
						configObj.maxHeadLeftMenu();
					}
				}, 500)
			}
		})
		var w = $(window).width();
		if(configObj.theme.menu != 1){
			// 菜单设置为移动端
		    if(w < configObj.minWidth){
		    	$('.lay-hiden-menu').attr('lay-id', '');
		    }else{
		    	$('.lay-hiden-menu').attr('lay-id', 1);
		    }
		    configObj.hidenMenu('.lay-hiden-menu');
		}else{
			if(w < configObj.minWidth){
				if(configObj.theme.logo){
					$('.layui-logo').removeClass('layui-logo-fixed');
				}
				$('.layui-logo').css({'width': '0px' , 'left': '-50px'});
			}else{
				if(configObj.theme.logo){
					$('.layui-logo').addClass('layui-logo-fixed');
				}else{
					$('.layui-logo').css({'width': '220px' , 'left': '0px'});
				}
			}
		}
	},
}


