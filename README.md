# layMenu
#### 日志
[前往更新日志](https://gitee.com/yuanyongqiang/lay-menu/blob/master/LOG.md)
#### 介绍
一个简单通用的主页前端框架

#### 软件架构
一个基于layui、jquery实现的html主页架构


#### 安装教程

下载下来按照index.html使用
【测试：下载下来后使用HBuilder打开预览或其他工具，因为加载菜单和页面不用服务打开的话会有跨越问题】

#### 使用说明

比较简单三步就可以用起来了

##### 一、初始化配置

```
// 初始化配置
configObj.init({
    scrollTop: true, // 回到顶部
    layui: '2.6.8', // 请根据使用版本修改此处（必须三位数）
    search: '搜索...', // 布尔/字符串（字符串代表提示；如果设置为false，说明不需要显示，那么请删除index.html中的class="lay-tree-menu"标签）
    //iframe: true, // 是否开启iframe模式
    /*theme: {// 主题设置内容
        'theme': 16, // 主题样式
        'menu': 2, // 菜单类型
        'page': true, // 标签页
        'pageType': false, // 标签页圆角
        'pageIcon': false, // 标签页图标
        'logo': false, // 固定logo
        'refresh': false, // 点击菜单刷新
        'radius': true, // 选中菜单圆角
        'anim': 2, // 页面动画效果
        'leftScroll': true, // 菜单滚动条显示
        'leftCloseChild': true // 关闭其他展开子菜单
    }, */
});
```
##### 二、渲染菜单数据
data是菜单数据
```
configObj.loadMenu(data);
```
##### 三、主题修改回调
每次修改主题时就会触发这个回调操作

```
configObj.themeCallback = function(data){
    console.log('主题设置：', JSON.stringify(data));
}

```
##### 跳转页面/切换标签页
非iframe下使用方式
```
configObj.setTabUrl({
    mid: 100001,
    url: 'page/xxx.html',
    mname: '某某菜单',
})
```
iframe下使用方式
```
window.parent.configObj.setTabUrl({
    mid: 100001,
    url: 'page/xxx.html',
    mname: '某某菜单',
})
```
已经封装好了，以上就可以使用了！

##### 主题存储/获取
    一般主题我们可以对用户进行数据库一对一存储，如果觉得没有必要存储到数据库的话，我们可以存储到浏览器中，如下：
存储浏览器中的localStorage
```
var theme = {'theme': 16, 'menu': 2, 'page': true, 'logo': false, 'refresh': false, 'radius': true, 'anim': 2};
localStorage.setItem('lay_menu_theme', JSON.stringify(theme));
```
获取存储到localStorage的主题
```
var value = localStorage.getItem('lay_menu_theme');
var theme = JSON.parse(value);
```



#### 功能说明
1. 兼容iframe和非iframe模式
2. 兼容移动端模式，默认最小宽度1100开始（响应式）
3. 支持21种主题更换
4. 可开启/关闭标签页，设定点击左侧菜单是否需要刷新当前页
5. 以及页面显示动画效果自定义
6. 标签页开启后，超出将会自动定位当前标签页位置，可操作左右切换显示标签页，可关闭其他、左 边、右边标签页，刷新当前
7. 支持菜单搜索
8. 自定义左侧菜单、头部菜单、头部和左侧混合使用菜单
9. 自定义是否固定logo

#### 效果图

##### PC端
默认左侧式菜单
![输入图片说明](img/1701483339013.jpg)
可在主题处设置头部菜单
![输入图片说明](img/1701483295019.jpg)
或者混合式菜单
![输入图片说明](img/1701483907746.jpg)

##### 移动端
![输入图片说明](img/1701483673530.jpg)
![输入图片说明](img/1701483772968.jpg) 
